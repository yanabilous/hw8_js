//Завдання

// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000

const paragraphs = document.querySelectorAll("p");

paragraphs.forEach(p => {
    p.style.color = "#ff0000";
});

// 2 Знайти елемент із id="optionsList". Вивести у консоль.
// Знайти батьківський елемент та вивести в консоль.
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

const elementId = document.querySelector("#optionsList");
console.log(elementId);
const parent = elementId.parentNode;
console.log(parent);
const childNodes = elementId.childNodes;
console.log(childNodes);

childNodes.forEach(node => {
    console.log(`nodeName: ${node.nodeName}, nodeType: ${node.nodeType}`);
});

// 3 Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// This is a paragraph

const textElement = document.querySelector("#testParagraph");
textElement.innerHTML = `This is a paragraph`;

// 4 Отримати елементи
// , вкладені в елемент із класом main-header і вивести їх у консоль.
// Кожному з елементів присвоїти новий клас nav-item.

let listElem = document.querySelector(".main-header");
console.log(listElem);
let children = listElem.children;
console.log(children);

for (let i = 0; i < children.length; i++) {
    children[i].className = children[i].className + " nav-item";
    console.log(children[i]);
}


// 5  Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const getElem = document.querySelectorAll(".section-title");
console.log(getElem);

for (let i = 0; i < getElem.length; i++) {
    getElem[i].classList.remove("section-title");
}
